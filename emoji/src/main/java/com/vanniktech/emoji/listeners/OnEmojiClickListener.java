package com.vanniktech.emoji.listeners;

import android.support.annotation.NonNull;
import android.view.View;

import com.vanniktech.emoji.emoji.Emoji;



public interface OnEmojiClickListener {
  void onEmojiClick(@NonNull View emoji, @NonNull Emoji imageView);
}
