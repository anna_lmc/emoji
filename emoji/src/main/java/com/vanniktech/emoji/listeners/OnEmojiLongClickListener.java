package com.vanniktech.emoji.listeners;


import android.support.annotation.NonNull;
import android.view.View;

import com.vanniktech.emoji.emoji.Emoji;

public interface OnEmojiLongClickListener {
  void onEmojiLongClick(@NonNull View view, @NonNull Emoji emoji);
}
